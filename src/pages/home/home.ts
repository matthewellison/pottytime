
import { Component, OnInit } from '@angular/core';
// import { Storage } from '@ionic/storage';
import { NavController, AlertController, ToastController } from 'ionic-angular';
import { PottyTime } from '../../resources/pottytime';
import * as moment from 'moment';

import { PottyTimeService } from '../../resources/pottyTimeService';
// import * as StopWatch from 'timer-stopwatch';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})



export class HomePage implements OnInit {
  TimeStarted;
  showTimeStarted = false;
  //listOfPottyTimesForToday: PottyTime[] = [];
  //currentOptions: UserOptions;
  stopwatch;
  showCard = false;

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public pottyTimeService: PottyTimeService) {
    var currentTime = moment().format();
    this.pottyTime.timeStarted = currentTime;
    this.showTimeStarted = false;
    document.addEventListener('resume', () => {
      this.onResume();
    });

  }

  onResume() {
    this.pottyTimeService.init().then(() => {
      if (moment().isAfter(this.pottyTimeService.nextAlertTime)) {
        this.showCard = true;
      } else {
        this.showCard = false;
      }
    });
  }

  ngOnInit() {
    console.log('init');
    this.pottyTimeService.init().then(() => {
      if (moment().isAfter(this.pottyTimeService.nextAlertTime)) {
        this.showCard = true;
      }
    });

  }

  RemoveItem(item: PottyTime) {
    let prompt = this.alertCtrl.create({
      title: 'Are you Sure you want to remove this Item?',
      message: item.cleanName(),
      buttons: [
        {
          text: 'Cancel',
          handler: () => { }
        },
        {
          text: 'Remove Item',
          handler: () => {
            this.pottyTimeService.removePottyTime(item);
          }
        }
      ]
    });
    prompt.present();
  }

  PottyTime() {
    //Schedule a single notification
    this.showTimeStarted = true;
    this.TimeStarted = this.pottyTime.timeStarted;
    var newPottyTime = new PottyTime(this.pottyTime.timeStarted);
    this.showCard = true;

    this.pottyTimeService.addPottyTime(newPottyTime);
    this.pottyTimeService.nextAlertTime = this.pottyTimeService.Options.getNextAlertTime();
    this.pottyTimeService.scheduleNotifications();

    let toast = this.toastCtrl.create({
      //message: 'Next Potty Time Alert has been schedule for ' + moment(this.pottyTimeService.Options.getNextAlertTime()).format('lll'),
      message: "Your potty time reminders have been scheduled for today based on your options",
      duration: 10000,
      showCloseButton: true,
      position: 'top',
      closeButtonText: 'Ok'
    });

    toast.present();

  }

  pottyTime = {
    timeStarted: '08:00',
  }
}

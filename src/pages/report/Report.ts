import { PottyTimeService } from '../../resources/pottyTimeService';
import { Mailgun } from '../../resources/mailgun';
import { Component, OnInit } from '@angular/core';
// import { Storage } from '@ionic/storage';
import { NavController } from 'ionic-angular';
// import { PottyTime } from '../../resources/pottytime';

@Component({
  selector: 'page-report',
  templateUrl: 'report.html'
})
export class ReportPage implements OnInit {

  constructor(public navCtrl: NavController,
    public mailgun: Mailgun,
    public pottyTimeService: PottyTimeService) {
  }
  ngOnInit() {
  }

  sendEmail(){
    this.mailgun.send("matthewcellison@gmail.com","test email","I hope this works");
  }
}

import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { Options } from '../options/options';
import { ReportPage } from '../report/Report'

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = HomePage;
  tab2Root: any = Options;
  tab3Root: any = ReportPage;

  constructor() {

  }
}

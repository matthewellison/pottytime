
import { UserOptions } from '../../resources/userOptions';
import { PottyTimeService } from '../../resources/pottyTimeService';
import { Component } from '@angular/core';
import { NavController, AlertController, ToastController } from 'ionic-angular';

@Component({
  selector: 'page-options',
  templateUrl: 'options.html'
})
export class Options {
  relative;
  somethingChange = false;
  currentOptions: UserOptions = new UserOptions(this.pottyService.defaultOptions);

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private pottyService: PottyTimeService) {

    this.currentOptions = pottyService.Options;
    //pottyService.getOptions().then(r => this.currentOptions = r);
  }

  public changed() {
    let toast = this.toastCtrl.create({
      message: 'Time has been updated',
      duration: 3000,
      showCloseButton: true,
      position: 'top',
      closeButtonText: 'Ok'
    });

    toast.present();
    console.log('Current Options');
    console.log(this.currentOptions);
    this.pottyService.saveOptions(this.currentOptions);
  }


  public DeleteAllData() {


    let prompt = this.alertCtrl.create({
      title: 'Are you Sure you want to delete all the data?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => { }
        },
        {
          text: 'Delete all Data',
          handler: () => {
            //this.storage.set('ThePottyTimes', null);
            this.pottyService.deleteAllData();
          }
        }
      ]
    });
    prompt.present();
  }

  CancelAllAlerts() {
    this.pottyService.cancelAllAlerts();

    let toast = this.toastCtrl.create({
      message: "All alerts have been cancelled",
      duration: 10000,
      showCloseButton: true,
      position: 'top',
      closeButtonText: 'Ok'
    });

    toast.present();

    
  }
}

import { PottyTimeService } from '../resources/pottyTimeService';
import { Mailgun } from '../resources/mailgun';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Options } from '../pages/options/options';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ReportPage } from '../pages/report/Report'
import { Storage } from '@ionic/storage';




@NgModule({
  declarations: [
    MyApp,
    Options,
    HomePage,
    TabsPage,
    ReportPage,
  ],
  imports: [
    IonicModule.forRoot(MyApp),
  ],

  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Options,
    HomePage,
    TabsPage,
    ReportPage,
  ],
  providers: [PottyTimeService, Mailgun, {
    provide: ErrorHandler,
    useClass: IonicErrorHandler
  }, Storage]
})
export class AppModule {
  constructor() {
    document.addEventListener('pause', () => {
      console.log('pause')
    });
    document.addEventListener('resume', () => {
      console.log('resume');

    });
  }
}

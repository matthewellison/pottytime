import { Injectable } from '@angular/core';
import {Http, Request, RequestMethod, Headers} from "@angular/http";
 
@Injectable()
export class Mailgun {
 
    http: Http;
    mailgunUrl: string;
    mailgunApiKey: string;
 
    constructor(http: Http) {
        this.http = http;
        this.mailgunUrl = "sandbox530edb658b4f4e9985722d7b246f98e6.mailgun.org";
        this.mailgunApiKey = window.btoa("api:key-6876490c211b941ef95fd59a38c6fecd");
    }
 
    send(recipient: string, subject: string, message: string) {
        var requestHeaders = new Headers();
        requestHeaders.append("Authorization", "Basic " + this.mailgunApiKey);
        requestHeaders.append("Content-Type", "application/x-www-form-urlencoded");
        this.http.request(new Request({
            method: RequestMethod.Post,
            url: "https://api.mailgun.net/v3/" + this.mailgunUrl + "/messages",
            body: "from=matthewcellison@gmail.com&to=" + recipient + "&subject=" + subject + "&text=" + message,
            headers: requestHeaders
        }))
        .subscribe(success => {
            console.log("SUCCESS -> " + JSON.stringify(success));
        }, error => {
            console.log("ERROR -> " + JSON.stringify(error));
        });
    }
}

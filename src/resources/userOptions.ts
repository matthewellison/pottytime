import * as moment from 'moment';
export class UserOptions {
    constructor(object) {
        if (object != null) {
            this.primaryAlertTime = object.primaryAlertTime;
            this.secondaryAlertTime = object.secondaryAlertTime;
        } else {
            this.primaryAlertTime = '02:00';
            this.secondaryAlertTime = '00:30';
        }

    }
    primaryAlertTime;;
    secondaryAlertTime;

    getNextAlertTime() {
        var m = moment("01/01/1900 " + this.primaryAlertTime);
        var hour = m.hours();
        var min = m.minute();
        return moment().add(hour, 'h').add(min, 'm').format();
    }
    getSecondaryAlertTimes(): Date[] {
        var dates = [];
        var m = moment("01/01/1900 " + this.secondaryAlertTime);
        var hour = m.hours();
        var min = m.minute();
        var secAlert = moment(this.getNextAlertTime()).add(hour, 'h').add(min, 'm');
        dates.push(secAlert.format());

        for (let i = 0; i <= 10; i++) {
            var moreAlerts = moment(dates[dates.length - 1]).add(hour, 'h').add(min, 'm');
            dates.push(moreAlerts.format());
        }
        //console.log(dates);
        return dates;
    }
    getNextAlertTimeFor(date: Date) {
        var m = moment("01/01/1900 " + this.primaryAlertTime);
        var hour = m.hours();
        var min = m.minute();
        return moment(date).add(hour, 'h').add(min, 'm').format('LLL');


    }

}
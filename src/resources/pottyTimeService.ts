
import { UserOptions } from '../resources/userOptions';
import { Injectable } from '@angular/core';
import { PottyTime } from './pottytime';
import { Storage } from '@ionic/storage';
import { LocalNotifications } from 'ionic-native';
import * as moment from 'moment';

@Injectable()
export class PottyTimeService {
    AllPottyTimes: PottyTime[] = [];
    pottyTimesForToday: PottyTime[] = [];
    defaultOptions: UserOptions = new UserOptions({ primaryAlertTime: '02:00', secondaryAlertTime: '00:30' });
    Options: UserOptions;
    nextAlertTime;
    constructor(public storage: Storage) {

    }
    init() {
        console.log('p-init');
        this.Options = new UserOptions(this.defaultOptions);
        return this.getAllPottyTimes().then(r => this.AllPottyTimes = r).then(() => {
            this.pottyTimesForToday = this.getPottyTimesForToday();
            return this.getOptions().then(r => this.Options = r).then(() => {
                this.nextAlertTime = this.Options.getNextAlertTimeFor(this.getMostRecentTime());
                console.log('next alert');
                console.log(this.nextAlertTime);
            });
        });

    }
    getPottyTimesForToday() {
        return this.AllPottyTimes
            .filter(function (e) { return e.IsToday() })
            .sort((a, b) => a.pottytime.getTime() - b.pottytime.getTime());
    }

    getAllPottyTimes() {

        return this.storage.get('ThePottyTimes').then(v => {
            if (v) {
               return JSON.parse(v).map(function (e) {
                    return new PottyTime(e.pottytime)
                }).sort((a, b) => a.pottytime.getTime() - b.pottytime.getTime());
            }else{
                return [];
            }

        });

    }

    addPottyTime(PottyTime) {
        console.log('saved');
        console.log(PottyTime);
        this.AllPottyTimes.push(PottyTime);
        return this.saveAllPottyTimes();

    }
    removePottyTime(PottyTime) {
        this.AllPottyTimes.splice(this.AllPottyTimes.indexOf(PottyTime), 1);
        return this.saveAllPottyTimes().then(() => {
        });
    }



    saveOptions(options: UserOptions) {
        this.Options = options;
        return this.storage.set('options', JSON.stringify(options));
    }

    deleteAllData() {
        this.Options = new UserOptions(this.defaultOptions);
        this.AllPottyTimes = [];
        this.pottyTimesForToday = [];
        this.storage.set('options', JSON.stringify(this.Options));
        this.storage.set('ThePottyTimes', "[]");
    }

    getMostRecentTime() {
        var mr = this.AllPottyTimes[this.AllPottyTimes.length - 1];
        if (mr instanceof (PottyTime)) {
            return this.AllPottyTimes[this.AllPottyTimes.length - 1].pottytime;
        }
        return undefined;
        //return this.AllPottyTimes[this.AllPottyTimes.length - 1].pottytime;
    }
    updateNextAlert() {
        this.nextAlertTime = this.Options.getNextAlertTime();
    }
    scheduleNotifications() {
        console.log('Notifications');
        let firstNotificationTimeNew = this.Options.getNextAlertTime();
        console.log(moment(firstNotificationTimeNew).format('lll'));
        let notification = {
            id: 1,
            title: 'POTTY TIME',
            text: 'Time to make that boy go potty',
            //at: new Date(new Date().getTime() + 3000),
            at: new Date(firstNotificationTimeNew)
        };

        let secondaryNotifications = [];
        this.Options.getSecondaryAlertTimes().forEach((e, i) => {
            secondaryNotifications.push({
                id: i,
                title: 'POTTY TIME - Overdue',
                text: 'Time to make that boy go potty NOW. Dont make me wait!',
                //at: new Date(new Date().getTime() + 3000),
                at: new Date(e)
            });
        })

        LocalNotifications.cancelAll().then(() => {
            LocalNotifications.schedule(notification);
            LocalNotifications.schedule(secondaryNotifications);
        })
    }

    cancelAllAlerts() {
        LocalNotifications.cancelAll();
    }

    private saveAllPottyTimes() {
        return this.storage.set('ThePottyTimes',
            JSON.stringify(this.AllPottyTimes)
        ).then(() => {
            //this.pottyTimesForToday.push(PottyTime);
            this.pottyTimesForToday = this.getPottyTimesForToday();
        });

    }

    private getOptions() {
        return this.storage.get('options').then(o => new UserOptions(JSON.parse(o)));
    }
}
import * as moment from 'moment';
export class PottyTime {
    constructor(starttime) {
        this.pottytime = new Date(starttime);
    }
    pottytime: Date;

    cleanName() {
        //console.log(this.pottytime);
        return moment(this.pottytime).format('MM/DD/YYYY h:mm:ss a');
    }

    justTime() {
        return moment(this.pottytime).format('h:mm:ss a');
    }

    IsToday() {
        return moment(this.pottytime).isSame(new Date(), "day");
        //var today = moment(moment(new Date()).format('MM/DD/YYY'));
        //return moment(this.pottytime).isSameOrAfter(today);

        //return this.pottytime > today;
        //return moment(this.pottytime).format('h:mm:ss a');

    }
}